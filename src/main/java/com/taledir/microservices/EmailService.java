package com.taledir.microservices;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
public class EmailService {

  Logger logger = LoggerFactory.getLogger(EmailService.class);

  @Autowired
  private JavaMailSender javaMailSender;

  protected void sendEmail(String email, String subject, String html) {

    MimeMessage msg = javaMailSender.createMimeMessage();

    try {
      MimeMessageHelper helper = new MimeMessageHelper(msg);
      helper.setTo(email);
      helper.setSubject(subject);
      helper.setText(html, true);
      javaMailSender.send(msg);

    } catch (MessagingException e1) {
      throw new EmailBadRequestException("There was an issue with sending email");
    }
  }

}